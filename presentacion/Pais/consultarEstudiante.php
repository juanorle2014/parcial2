<?php 
$filtro = 'AF';
$pais= new Pais($filtro);
$pais->consultarName();
$reporte= new Reporte('', '', '', '', '', '', $filtro);
$reporte->SumarCAcumulados();
$reporte->SumarMAcumulados();
?>

<div class="container">
	<table class="table">
		<thead>
			<tr>
				<th scope="col">Region</th>
				<th scope="col">Codigo</th>
				<th scope="col">Nombre</th>
				<th scope="col">Casos Acumulados</th>
				<th scope="col">Muertes Acumuladas</th>
			</tr>
		</thead>
		<tbody>
   

<tr>
<td><?php echo $pais->getRegion()->getNombre() ?></td>
 <td><?php echo $pais->getId() ?></td>
 <td><?php echo $pais->getNombre() ?></td>
 <td><?php echo $reporte->getCasos_acumulados() ?></td>
 <td><?php echo $reporte->getMuertes_acumulados() ?></td>
</tr>


  </tbody>
	</table>

</div>
