<?php
include 'presentacion/navbar.php';
$Pais = new Pais();
$Paises = $Pais -> consultar();
?>

<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Covid-19</h5>
				<div class="card-body">
					<div class="row">
						<div class="col-4"></div>
						<div class="col-4 text-center">
						<div class="mb-3">
							<label class="form-label">Pais</label>
							<select class="form-select" id="filtro">
							<?php 
							foreach ($Paises as $paisActual){
							    echo "<option value='" . $paisActual -> getId() . "'>" . $paisActual -> getNombre() . "</option>";
							}							
							?>							
							</select>
						</div>
						</div>	
					</div>
					<div class="row">
						<div class="col">
							<div id="resultados"></div>
						</div>
					</div>				
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$( "#filtro" ).change(function() {
  filtro = $( "#filtro" ).val(); 
      url = "indexAjax.php?pid=<?php echo base64_encode("presentacion/Pais/buscarAjax.php")?>&filtro=" + filtro;      
      $( "#resultados" ).load(url);    

});
</script>


