<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/ReporteDAO.php';

class Reporte{
    private $id;
    private $fecha;
    private $nuevos_casos;
    private $casos_acumulados;
    private $nuevas_muertes;
    private $muertes_acumulados;
    private $pais;
    private $conexion;
    private $ReporteDAO;
    
    public function getId(){
        return $this->id;
    }


    public function getFecha(){
        return $this->fecha;
    }

    public function getNuevos_casos(){
        return $this->nuevos_casos;
    }

    public function getCasos_acumulados(){
        return $this->casos_acumulados;
    }


    public function getNuevas_muertes(){
        return $this->nuevas_muertes;
    }


    public function getMuertes_acumulados(){
        return $this->muertes_acumulados;
    }
    public function getPais(){
        return $this->pais;
    }

   

    public function Reporte($id="", $fecha="", $nuevos_casos="", $casos_acumulados="", $nuevas_muertes="", $muertes_acumulados="", $pais=""){
        $this -> id = $id;
        $this -> fecha = $fecha;
        $this -> nuevos_casos = $nuevos_casos;
        $this -> casos_acumulados = $casos_acumulados;
        $this -> nuevas_muertes = $nuevas_muertes;
        $this -> muertes_acumulados = $muertes_acumulados;
        $this -> pais = $pais;
        $this -> conexion = new Conexion();
        $this -> ReporteDAO = new ReporteDAO($this -> id, $this -> fecha, $this -> nuevos_casos, $this -> casos_acumulados, $this -> nuevas_muertes, $this -> muertes_acumulados, $this -> pais);
    }
    
    
    public function buscar($filtro){
        $this -> conexion -> abrir();
        $this -> ReporteDAO -> buscar($filtro);
        $this -> conexion -> ejecutar($this -> ReporteDAO -> buscar($filtro));
        $reportes = array();
        while(($registro = $this -> conexion -> extraer()) != null){
            $country = new Pais($registro[6]);
            $country-> consultarName();
            $this -> pais = $country;
            $reporte = new Reporte($registro[0], $registro[1], $registro[2], $registro[3], $registro[4], $registro[5], $this-> pais);
            array_push($reportes, $reporte);
        }
        $this -> conexion -> cerrar();
        return  $reportes;
    }

    public function SumarCAcumulados(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ReporteDAO -> SumarCAcumulados());
        $this -> conexion -> cerrar();
        $datos = $this -> conexion -> extraer();
        $this -> casos_acumulados = $datos[0];

    }

    public function SumarMAcumulados(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ReporteDAO -> SumarCAcumulados());
        $this -> conexion -> cerrar();
        $datos = $this -> conexion -> extraer();
        $this -> muertes_acumulados = $datos[0];

    }
    
    
    
}

?>