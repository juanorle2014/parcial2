<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/PaisDAO.php';

class Pais{
    private $id;
    private $nombre;
    private $region;
    private $conexion;
    private $PaisDAO;
    
    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }
   
    public function getRegion()
    {
        return $this->region;
    }

   

    public function Pais($id="", $nombre="", $region=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> region = $region;
        $this -> conexion = new Conexion();
        $this -> PaisDAO = new PaisDAO($this -> id, $this -> nombre, $this -> region);
    }
    

    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> PaisDAO -> consultar());
        $Paises = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            
            array_push($Paises, new Pais($resultado[0], $resultado[1]));
        }
        $this -> conexion -> cerrar();
        return $Paises;
    }

    public function consultarName(){
        $this -> conexion -> abrir();
        
        $this -> conexion -> ejecutar($this -> PaisDAO -> consultarName());
        $this -> conexion -> cerrar();
        $datos = $this -> conexion -> extraer();
        $this -> nombre = $datos[1];
        $this -> id =$datos[0];
        $aux = new Region($datos[2]);
        $aux->consultarName();
        $this -> region = $aux;   
    }
    
   
    
    
}

?>