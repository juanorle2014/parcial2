<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/RegionDAO.php';

class Region{
    private $id;
    private $nombre;
    private $conexion;
    private $RegionDAO;
    
    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

   

    public function Region($id="", $nombre=""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> conexion = new Conexion();
        $this -> RegionDAO = new RegionDAO($this -> id, $this -> nombre);
    }
    
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> RegionDAO -> consultar());
        $Regions = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            
            array_push($Regions, new Region($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
        }
        $this -> conexion -> cerrar();
        return $Regions;
    }

    public function consultarName(){
        $this -> conexion -> abrir();
        //echo $this -> PaisDAO -> consultarName();
        $this -> conexion -> ejecutar($this -> RegionDAO -> consultarName());
        $this -> conexion -> cerrar();
        $datos = $this -> conexion -> extraer();
        $this -> nombre = $datos[1];
        $this -> id =$datos[0];
          
    }
    
    
    
}

?>